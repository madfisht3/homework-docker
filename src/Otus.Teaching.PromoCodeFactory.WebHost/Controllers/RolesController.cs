﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.DTO.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Роли сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RolesController
    {
        private readonly IRepository<Role> _rolesRepository;
        private readonly IMapper _mapper;

        public RolesController(IRepository<Role> rolesRepository,
            IMapper mapper)
        {
            _rolesRepository = rolesRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все доступные роли сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<RoleResponse>> GetRolesAsync()
        {
            var roles = await _rolesRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<RoleResponse>>(roles);
        }
    }
}