namespace Otus.Teaching.PromoCodeFactory.DataAccess.Utils
{
    public interface IEFDbInitializer
    { 
        void Initialize();
    }
}