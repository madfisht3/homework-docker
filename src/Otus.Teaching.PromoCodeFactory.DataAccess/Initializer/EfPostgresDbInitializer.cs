﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Utils;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Initializer
{
    public class EfPostgresDbInitializer : IEFDbInitializer
    {
        private readonly PromoCodeDataContext _dataContext;
        private readonly ILogger<EfPostgresDbInitializer> _logger;

        public EfPostgresDbInitializer(PromoCodeDataContext dataContext,
            ILogger<EfPostgresDbInitializer> logger)
        {
            _dataContext = dataContext;
            _logger = logger;
        }
        public void Initialize()
        {
            if (_dataContext.Database.GetPendingMigrations().Any())
            {
                _dataContext.Database.Migrate();
                _logger.LogInformation("Migrations were applied.");

                _dataContext.Roles.AddRange(FakeDataFactory.Roles);
                _dataContext.Preferences.AddRange(FakeDataFactory.Preferences);
                _dataContext.Customers.AddRange(FakeDataFactory.Customers);
                _dataContext.SaveChanges();

                _dataContext.Employees.AddRange(FakeDataFactory.Employees.Select(e =>
                {
                    e.Role = _dataContext.Roles.Single(r => r.Id == e.Role.Id);
                    return e;
                }));
                _dataContext.SaveChanges();
            }
        }
    }
}
